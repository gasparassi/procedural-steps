<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::get(
    '/',
    function () {
        return response()
            ->json(
                [
                    'message' => 'Proceedings API',
                    'status' => 'Connected',
                    'statusCode' => 200,
                ],
                200
            );
    }
);

Route::group([
    //    'middleware' => 'users',
    'prefix' => 'v1',
], function () {

    Route::group([
        'prefix' => 'departments',
    ], function () {
        Route::post('/', 'DepartmentController@store')->name('departamentos.store');
        Route::get('/', 'DepartmentController@index')->name('departamentos.index');
        Route::get('/user-register', 'DepartmentController@indexUserRegister')->name('departamentos.index2');
        Route::get('/{id}', 'DepartmentController@show')->name('departamentos.show');
        Route::put('/{id}', 'DepartmentController@update')->name('departamentos.update');
        Route::delete('/{id}', 'DepartmentController@destroy')->name('departamentos.delete');
    });

    Route::group([
        'prefix' => 'users',
    ], function () {
        Route::post('/register', 'Auth\RegisterController@register')->name('user.register');
        Route::post('/login', 'Auth\LoginController@login')->name('user.login');

        Route::post('/refresh-token', 'Auth\RefreshTokenController@refreshToken')->name('user.refreshToken');
        Route::post('/logout', 'Auth\LogoutController@logout')->name('user.logout');
        Route::get('/', 'UserController@index')->name('user.index');
        Route::get('/{id}', 'UserController@show')->name('user.show');
        Route::put('/{id}', 'UserController@update')->name('user.update');
        Route::delete('/{id}', 'UserController@destroy')->name('user.delete');
    });

    Route::group([
        'prefix' => 'proccesses',
    ], function () {
        Route::post('/', 'ProccessController@store')->name('processos.store');
        Route::get('/', 'ProccessController@index')->name('processos.index');
        Route::get('/{id}', 'ProccessController@show')->name('processos.show');
        Route::put('/{id}', 'ProccessController@update')->name('processos.update');
        Route::delete('/{id}', 'ProccessController@destroy')->name('processos.delete');
    });

    Route::group([
        'prefix' => 'procedurals',
    ], function () {
        Route::post('/', 'ProceduralController@store')->name('tramites.store');
        Route::get('/', 'ProceduralController@index')->name('tramites.index');
        Route::get('/{id}', 'ProceduralController@show')->name('tramites.show');
        Route::put('/{id}', 'ProceduralController@update')->name('tramites.update');
        Route::delete('/{id}', 'ProceduralController@destroy')->name('tramites.delete');
    });
});
