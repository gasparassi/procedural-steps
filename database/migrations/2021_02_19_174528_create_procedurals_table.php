<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProceduralsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procedurals', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('proccess_id');
            $table->foreignId('department_id');
            $table->string('motivo', 50)->comment('Motivo da tramitação');
            $table->string('observacoes', 150)->comment('Observações adicionais sobre o trâmite');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('proccess_id')->references('id')->on('proccesses')->cascadeOnDelete();
            $table->foreign('department_id')->references('id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procedurals');
    }

}
