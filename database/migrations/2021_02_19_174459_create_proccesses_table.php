<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProccessesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proccesses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->id();
            $table->foreignId('user_id');
            $table->integer('numero_processo')->comment('Número do processo');
            $table->integer('ano_processo')->comment('Ano do processo');
            $table->string('tipo')->comment('ARP, Aditivo, Apostilamento, Contrato');
            $table->text('objeto', 100)->comment('Objeto do requerimento');
            $table->integer('ano_tipo')->nullable()->comment('Ano do aditivo ou do apostilamento ou da ARP');
            $table->string('nome_contratado', 100)->nullable();
            $table->integer('numero_tipo')->nullable()->comment('Número do aditivo ou do apostilamento ou da ARP');
            $table->integer('numero_contrato')->nullable()->comment('Se for aditivo ou apostilamento, qual o número do contrato');
            $table->integer('ano_contrato')->nullable()->comment('Se for aditivo ou apostilamento, qual o ano do contrato');
            $table->string('data_vencimento_contrato')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proccesses');
    }

}
