<?php

use Illuminate\Database\Seeder;

class ProceduralSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\ProceduralModel::class, 100)->create();
    }

}
