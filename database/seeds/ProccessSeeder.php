<?php

use Illuminate\Database\Seeder;

class ProccessSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\ProccessModel::class, 15)->create();
    }

}
