<?php

use Illuminate\Database\Seeder;

class DepartamentSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\DepartmentModel::class, 5)->create();
    }

}
