<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\ProccessModel;
use App\Models\UserModel;
use Faker\Generator as Faker;

$factory->define(ProccessModel::class, function (Faker $faker) {
    $types = [
        'Aditivo',
        'Contrato',
        'Ata de Registro de Preço',
        'Termo de Fomento',
        'Termo de Cooperação',
    ];
    $typesKeys = array_rand($types);
    return [
        'numero_processo' => $faker->unique()->randomNumber(5),
        'data_vencimento_contrato' => $faker->dateTimeBetween('now', '2025-12-31'),
        'ano_processo' => $faker->year('now'),
        'tipo' => $types[$typesKeys],
        'ano_tipo' => $faker->year('now'),
        'numero_tipo' => $faker->randomNumber(2),
        'numero_contrato' => $faker->unique()->randomNumber(2),
        'ano_contrato' => $faker->year('now'),
        'objeto' => $faker->realText($faker->numberBetween(10,20)),
        'nome_contratado' => $faker->company,
        'user_id' => function () {
            return UserModel::all()->random()->id;
        }
    ];
});
