<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DepartmentModel;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(DepartmentModel::class, function (Faker $faker) {
    return [
        'name' => $faker->companySuffix,
        'icon' => Str::random(5),
        'color' => $faker->colorName,
    ];
});
