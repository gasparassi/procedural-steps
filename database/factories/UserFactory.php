<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\UserModel;
use App\Models\DepartmentModel;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(UserModel::class, function (Faker $faker) {
    return [
        'department_id' => function () {
            return DepartmentModel::all()->random()->id;
        },
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'status' => 1,
        'isAdmin' => 0,
        'password' => Hash::make('12345678'), // password
        'remember_token' => Str::random(60),
    ];
});
