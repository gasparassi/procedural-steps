<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\ProceduralModel;
use App\Models\UserModel;
use App\Models\ProccessModel;
use App\Models\DepartmentModel;
use Faker\Generator as Faker;

$factory->define(ProceduralModel::class, function (Faker $faker) {
    return [
        'motivo' => $faker->realText($faker->numberBetween(10, 20)),
        'observacoes' => $faker->realText($faker->numberBetween(10, 20)),
        'user_id' => function () {
            return UserModel::all()->random()->id;
        },
        'department_id' => function () {
            return DepartmentModel::all()->random()->id;
        },
        'proccess_id' => function () {
            return ProccessModel::all()->random()->id;
        },
    ];
});
