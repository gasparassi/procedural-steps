<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProccessUserResource;
use App\Http\Resources\ProccessUserDepartamentResource;

class ProccessProceduralsResource extends JsonResource
{

    protected $ordem = 0;
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => $this->id,
            'motivo' => $this->motivo,
            'observacoes' => $this->observacoes,
            'send_date' => $this->created_at,
            'destination_department' => new ProccessUserDepartamentResource($this->departament),
            'user_sent' => new ProccessUserResource($this->user),
        ];
    }

}
