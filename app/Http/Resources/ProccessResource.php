<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProccessUserResource;
use App\Http\Resources\ProccessProceduralsResource;
use Carbon\Carbon;

class ProccessResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $numero_ano_processo = $this->numero_processo . '/' . $this->ano_processo;
        
        return [
            'id' => $this->id,
            'data_cadastro' => Carbon::parse($this->created_at)->format('d.m.Y'),
            'numero_ano_processo' => $numero_ano_processo,
            'data_vencimento_contrato' => Carbon::parse($this->data_vencimento_contrato)->format('d.m.Y'),
            'tipo' => $this->tipo,
            'objeto' => $this->objeto,
            'numero_ano_tipo' => $this->getNumeroAnoTipo(),
            'nome_contratado' => $this->nome_contratado,
            'numero_ano_contrato' => $this->getNumeroAnoContrato(),
            'user_cadastro' => new ProccessUserResource($this->user),
            'last_procedural' => new ProccessProceduralsResource($this->getLastProcedural()),
        ];
    }

    private function getNumeroAnoTipo()
    {
        if ( $this->numero_tipo !== null ) {
            $numero_ano_tipo = $this->numero_tipo . '/' . $this->ano_tipo;
        } else {
            $numero_ano_tipo = null;
        }

        return $numero_ano_tipo;
    }

    private function getNumeroAnoContrato()
    {
        if ( $this->numero_contrato !== null ) {
            $numero_ano_contrato = $this->numero_contrato . '/' . $this->ano_contrato;
        } else {
            $numero_ano_contrato = $this->numero_tipo . '/' . $this->ano_tipo;;
        }

        return $numero_ano_contrato;
    }

}
