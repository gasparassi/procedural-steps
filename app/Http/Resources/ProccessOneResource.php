<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProccessUserResource;
use Carbon\Carbon;

class ProccessOneResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'data_cadastro' => Carbon::parse($this->created_at)->format('d.m.Y'),
            'numero_processo' => $this->numero_processo,
            'ano_processo' => $this->ano_processo,
            'tipo' => $this->tipo,
            'objeto' => $this->objeto,
            'numero_tipo' => $this->numero_tipo,
            'ano_tipo' => $this->ano_tipo,
            'nome_contratado' => $this->nome_contratado,
            'numero_contrato' => $this->numero_contrato,
            'ano_contrato' => $this->ano_contrato,
            'data_vencimento_contrato' => Carbon::parse($this->data_vencimento_contrato)->format('d.m.Y'),
            'user_cadastro' => new ProccessUserResource($this->user),
            'procedurals' => ProccessProceduralsResource::collection($this->procedurals),
        ];
    }

}
