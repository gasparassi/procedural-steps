<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProccessResource;
use App\Http\Resources\DepartmentResource;
use App\Http\Resources\UserResource;
use Carbon\Carbon;

class ProceduralResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'motivo' => $this->motivo,
//            'send_date' => Carbon::parse($this->created_at)->format('d.m.Y'),
            'send_date' => $this->created_at,
            'mother_proccess' => new ProccessResource($this->proccess),
            'user_envio' => new UserResource($this->user),
            'destiny_departament' => new DepartmentResource($this->departament),
        ];
    }

}
