<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class UserMiddleware extends Middleware
{

    /**
     * Exclude these routes from authentication check.
     *
     * @var array
     */
    protected $except = [
        'api/v1/users/login',
        'api/v1/users/register',
        'api/v1/departments/user-register',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            foreach ($this->except as $excludedRoute) {
                if ( $request->path() === $excludedRoute ) {
                    logger("Skipping $excludedRoute from auth check...");
                    return $next($request);
                }
            }

            logger('Authenticating... ' . $request->url());

            JWTAuth::parseToken()->authenticate();

            return $next($request);
        } catch (TokenExpiredException $ex) {
            logger('O token de acesso está expirado.');
            return $this->tokenExpired();
        } catch (TokenInvalidException $ex) {
            logger('O token de acesso é inválido.');
            return $this->tokenInvalid();
        } catch (TokenBlacklistedException $ex) {
            logger('O token de acesso está na lista negra.');
            return $this->tokenBlackListed();
        } catch (JWTException $ex) {
            logger('O token de acesso não foi encontrado.');
            return $this->tokenNotFound();
        }
    }

    private function tokenBlackListed()
    {
        return response()->json([
                    'messages' => 'Token de acesso está inutizado',
                    'code' => Response::HTTP_UNAUTHORIZED
                        ], Response::HTTP_UNAUTHORIZED);
    }

    private function tokenInvalid()
    {
        return response()->json([
                    'messages' => 'Token de acesso é inválido',
                    'code' => Response::HTTP_UNAUTHORIZED
                        ], Response::HTTP_UNAUTHORIZED);
    }

    private function tokenExpired()
    {
        return response()->json([
                    'messages' => 'Token de acesso está expirado',
                    'code' => Response::HTTP_UNAUTHORIZED
                        ], Response::HTTP_UNAUTHORIZED);
    }

    private function tokenNotFound()
    {
        return response()->json([
                    'messages' => 'Token de acesso não encontrado',
                    'code' => Response::HTTP_UNAUTHORIZED
                        ], Response::HTTP_UNAUTHORIZED);
    }

}
