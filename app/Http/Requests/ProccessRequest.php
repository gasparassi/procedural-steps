<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;

class ProccessRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'numero_processo' => 'required',
            'ano_processo' => 'required',
            'tipo' => 'required',
            'numero_tipo' => 'required',
            'ano_tipo' => 'required',
            'nome_contratado' => 'required|string',
        ];
    }

}
