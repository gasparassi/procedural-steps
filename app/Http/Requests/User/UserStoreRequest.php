<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequestCustom as FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'email' => 'required|regex:/^.+@.+$/i|string|email:rfc,dns,filter|unique:users',
            'password' => 'required|min:6|confirmed',
        ];
    }
}
