<?php

namespace App\Http\Requests\User;

use App\Models\UserModel;
use App\Http\Requests\FormRequestCustom as FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //                     /api/users/{1}
        //         segment->  / 1  / 2  / 3
//        $id = $this->segment(3);
        return [
            'name' => 'required|string|max:100',
            'email' => 'required|regex:/^.+@.+$/i|string|email:rfc,dns,filter',
            Rule::unique((new UserModel)->getTable())->ignore($this->route()->user->id ?? null),
            'password' => 'required|between:6,8|confirmed',
        ];
    }

}
