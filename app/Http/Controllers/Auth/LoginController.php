<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Http\Requests\User\UserLoginRequest;
use Illuminate\Http\Response;

class LoginController extends Controller
{

    protected $service;

    function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    protected function errorServer($th)
    {
        return response()->json([
                    'message' => 'Erro ao efetuar o login.',
                    'error' => $th->getMessage(),
                    'code' => Response::HTTP_INTERNAL_SERVER_ERROR
                        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function login(UserLoginRequest $request)
    {
        try {
            $credentials = $request->only('email', 'password');
            $token = $this->service->guard()->attempt($credentials);
            if ( $token ) {
                $user = $this->service->getUserByEmail($request->email);
                return $this->service->respondWithToken($user, $token);
            } else {
                return response()->json([
                            'error' => 'Credenciais inválidas.',
                            'code' => Response::HTTP_UNPROCESSABLE_ENTITY
                                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Throwable $th) {
            return $this->errorServer($th);
        }
    }

}
