<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Response;

class LogoutController extends Controller
{

    protected $service;

    function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            // Efetua logout e insere o token anteriormente utilizado na blacklist
            $this->service->guard()->logout(true);
            return response()->json([
                        'message' => 'Logout efetuado com sucesso.',
                        'code' => Response::HTTP_OK
                            ], Response::HTTP_OK);
        } catch (Exception $ex) {
            return response()->json([
                        'message' => 'Erro ao efetuar o logout.',
                        'error' => $ex->getMessage(),
                        'code' => Response::HTTP_INTERNAL_SERVER_ERROR
                            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
