<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserStoreRequest;
use App\Services\UserService;
use Illuminate\Http\Response;

class RegisterController extends Controller
{

    protected $service;

    function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\User\UserStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function register(UserStoreRequest $request)
    {
        $data = $this->service->store($request);
        try {
            if ( $data != null ) {
                return response()->json([
                            'data' => $data,
                            'code' => Response::HTTP_CREATED
                                ], Response::HTTP_CREATED);
            } else {
                return response()->json([
                            'messages' => 'Erro ao registrar usuário.',
                            'code' => Response::HTTP_UNPROCESSABLE_ENTITY
                                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Throwable $th) {
            return response()->json([
                        'message' => 'Erro ao efetuar o logout.',
                        'error' => $th->getMessage(),
                        'code' => Response::HTTP_INTERNAL_SERVER_ERROR
                            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
