<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Http\Resources\UserResource;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class RefreshTokenController extends Controller
{

    protected $service;

    function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken()
    {
        try {
            $user = new UserResource($this->service->guard()->user());
            $newToken = $this->service->guard()->refresh();

            return $this->service->respondWithToken($user, $newToken);
        } catch (TokenExpiredException $ex) {
            return response()->json([
                        'message' => 'Erro ao efetuar o logout.',
                        'error' => $ex->getMessage(),
                        'code' => $ex->getCode()
                            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
