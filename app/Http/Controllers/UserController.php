<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserUpdateRequest;
use App\Services\UserService;
use Illuminate\Http\Response;

class UserController extends Controller
{

    protected $service;

    function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    protected function errorServer($th)
    {
        return response()->json([
                    'message' => 'Erro ao efetuar o logout.',
                    'error' => $th->getMessage(),
                    'code' => Response::HTTP_INTERNAL_SERVER_ERROR
                        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->service->all();
        try {
            if ( count($data) > 0 ) {
                return response()->json([
                            'data' => $data,
                            'code' => Response::HTTP_OK
                                ], Response::HTTP_OK);
            } else {
                return response()->json([
                            'messages' => 'Nenhum usuário encontrado.',
                            'code' => Response::HTTP_NOT_FOUND
                                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->errorServer($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->service->getOne($id);
        try {
            if ( $data != null ) {
                return response()->json([
                            'data' => $data,
                            'code' => Response::HTTP_OK
                                ], Response::HTTP_OK);
            } else {
                return response()->json([
                            'messages' => 'Usuário não encontrado.',
                            'code' => Response::HTTP_NOT_FOUND
                                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->errorServer($th);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\User\UserUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        try {
            $data = $this->service->update($request, $id);
            if ( $data != null ) {
                return response()->json([
                            'data' => $data,
                            'code' => Response::HTTP_OK
                                ], Response::HTTP_OK);
            } else {
                return response()->json([
                            'messages' => 'Usuário não encontrado.',
                            'code' => Response::HTTP_NOT_FOUND
                                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->errorServer($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->service->delete($id);
            if ( $data ) {
                return response()->json([
                            'messages' => 'Usuário removido com sucesso.',
                            'code' => Response::HTTP_OK
                                ], Response::HTTP_OK);
            } else {
                return response()->json([
                            'messages' => 'Usuário não encontrado.',
                            'code' => Response::HTTP_NOT_FOUND
                                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->errorServer($th);
        }
    }

}
