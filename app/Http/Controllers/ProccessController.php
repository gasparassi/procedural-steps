<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Requests\ProccessRequest;
use App\Services\ProccessService;

class ProccessController extends Controller
{

    protected $service;

    function __construct(ProccessService $proccessService)
    {
        $this->service = $proccessService;
    }

    protected function errorServer($th)
    {
        return response()->json([
                    'message' => 'Erro ao efetuar o logout.',
                    'error' => $th->getMessage(),
                    'code' => Response::HTTP_INTERNAL_SERVER_ERROR
                        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->service->all();
        try {
            if ( $data !== null ) {
                return response()->json([
                            'data' => $data,
                            'code' => Response::HTTP_OK
                                ], Response::HTTP_OK);
            } else {
                return response()->json([
                            'messages' => 'Nenhum processo para listar.',
                            'code' => Response::HTTP_NOT_FOUND
                                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->errorServer($th);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\ProccessRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProccessRequest $request)
    {
        $data = $this->service->save($request);
        try {
            if ( $data !== null ) {
                return response()->json([
                            'data' => $data,
                            'code' => Response::HTTP_CREATED
                                ], Response::HTTP_CREATED);
            } else {
                return response()->json([
                            'messages' => 'Erro ao cadastrar o processo.',
                            'code' => Response::HTTP_UNPROCESSABLE_ENTITY
                                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Throwable $th) {
            return $this->errorServer($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->service->getOne($id);
        try {
            if ( $data !== null ) {
                return response()->json([
                            'data' => $data,
                            'code' => Response::HTTP_OK
                                ], Response::HTTP_OK);
            } else {
                return response()->json([
                            'messages' => 'Processo não encontrado.',
                            'code' => Response::HTTP_NOT_FOUND
                                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->errorServer($th);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\ProccessRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProccessRequest $request, $id)
    {
        try {
            $data = $this->service->update($id, $request);
            if ( $data !== null ) {
                return response()->json([
                            'data' => $data,
                            'code' => Response::HTTP_OK
                                ], Response::HTTP_OK);
            } else {
                return response()->json([
                            'messages' => 'Processo não encontrado.',
                            'code' => Response::HTTP_NOT_FOUND
                                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->errorServer($th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = $this->service->delete($id);
            if ( $data ) {
                return response()->json([
                            'messages' => 'Processo removido com sucesso.',
                            'code' => Response::HTTP_OK
                                ], Response::HTTP_OK);
            } else {
                return response()->json([
                            'messages' => 'Processo não encontrado.',
                            'code' => Response::HTTP_NOT_FOUND
                                ], Response::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            return $this->errorServer($th);
        }
    }

}
