<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProceduralModel extends Model
{

    protected $table = "procedurals";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'proccess_id', 'department_id',
        'motivo', 'observacoes',
    ];

    public function user()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'user_id');
    }

    public function proccess()
    {
        return $this->belongsTo('App\Models\ProccessModel', 'proccess_id', 'id');
    }

    public function departament()
    {
        return $this->hasOne('App\Models\DepartmentModel', 'id', 'department_id');
    }

}
