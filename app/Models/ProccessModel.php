<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProccessModel extends Model
{

    protected $table = "proccesses";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'numero_processo', 'ano_processo', 'tipo', 'numero_tipo', 'ano_tipo',
        'objeto', 'nome_contratado', 'numero_contrato', 'ano_contrato', 'data_vencimento_contrato'
    ];

    /**
     * Indicates if the resource's collection keys should be preserved.
     *
     * @var bool
     */
    public $preserveKeys = true;

    public function user()
    {
        return $this->belongsTo('App\Models\UserModel', 'user_id', 'id');
    }

    public function procedurals()
    {
        return $this->hasMany('App\Models\ProceduralModel', 'proccess_id', 'id');
    }

    public function getLastProcedural()
    {
        $lastId = $this->hasOne('App\Models\ProceduralModel', 'proccess_id', 'id')->max('id');
        return $this->hasOne('App\Models\ProceduralModel', 'proccess_id', 'id')->find($lastId);
    }

}
