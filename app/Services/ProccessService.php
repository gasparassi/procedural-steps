<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\ProccessRepository;
use App\Http\Resources\ProccessResource;
use App\Http\Resources\ProccessOneResource;

/**
 * Description of TramiteService
 *
 * @author eder
 */
class ProccessService
{

    protected $repository;

    function __construct(ProccessRepository $proccess)
    {
        $this->repository = $proccess;
    }

    private function requestOnly()
    {
        return $this->repository->makeModel()->getFillable();
    }

    public function save($data)
    {
        $fields = $this->repository->makeModel()->fill($data->only($this->requestOnly()));
        $proccess = $this->repository->create($fields->toArray());
        return $proccess !== null ? $proccess : null;
    }

    public function all()
    {
        $proccesses = $this->repository->all();
        return $proccesses !== null ? ProccessResource::collection($proccesses) : null;
    }

    public function getOne($id)
    {
        $proccess = $this->repository->getById($id);
        return $proccess !== null ? new ProccessOneResource($proccess) : null;
    }

    public function update($id, $data)
    {
        $fields = $this->repository->makeModel()->fill($data->only($this->requestOnly()));
        $proccess = $this->repository->makeModel()->find($id);
        return $proccess !== null ? $this->repository->updateById($id, $fields->toArray())->fresh() : null;
    }

    public function delete($id)
    {
        $proccess = $this->repository->makeModel()->find($id);
        if ( $proccess !== null ) {
            $this->repository->deleteById($id);
            return true;
        } else {
            return false;
        }
    }

}
