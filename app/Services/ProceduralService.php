<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\ProceduralRepository;
use App\Http\Resources\ProceduralResource;

/**
 * Description of TramiteService
 *
 * @author eder
 */
class ProceduralService
{

    protected $repository;

    function __construct(ProceduralRepository $procedural)
    {
        $this->repository = $procedural;
    }

    private function requestOnly()
    {
        return $this->repository->makeModel()->getFillable();
    }

    public function save($data)
    {
        $fields = $this->repository->makeModel()->fill($data->only($this->requestOnly()));
        $procedual = $this->repository->create($fields->toArray());
        return $procedual !== null ? $procedual : null;
    }

    public function all()
    {
        $procedurals = $this->repository->all();
        return $procedurals !== null ? ProceduralResource::collection($procedurals) : null;
    }

    public function getOne($id)
    {
        $procedural = $this->repository->getById($id);
        return $procedural !== null ? new ProceduralResource($procedural) : null;
    }

    public function update($id, $data)
    {
        $fields = $this->repository->makeModel()->fill($data->only($this->requestOnly()));
        $procedural = $this->repository->makeModel()->find($id);
        return $procedural !== null ? $this->repository->updateById($id, $fields->toArray())->fresh() : null;
    }

    public function delete($id)
    {
        $procedural = $this->repository->makeModel()->find($id);
        if ( $procedural != null ) {
            $this->repository->deleteById($id);
            return true;
        } else {
            return false;
        }
    }

}
