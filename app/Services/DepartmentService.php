<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Repositories\DepartmentRepository;
use App\Http\Resources\DepartmentResource;

/**
 * Description of TramiteService
 *
 * @author eder
 */
class DepartmentService
{

    protected $repository;

    function __construct(DepartmentRepository $departament)
    {
        $this->repository = $departament;
    }

    private function requestOnly()
    {
        return $this->repository->makeModel()->getFillable();
    }

    public function save($data)
    {
        $fields = $this->repository->makeModel()->fill($data->only($this->requestOnly()));
        $departament = $this->repository->create($fields->toArray());
        return $departament !== null ? $departament : null;
    }

    public function all()
    {
        $departaments = $this->repository->all();
        return $departaments !== null ? DepartmentResource::collection($departaments) : null;
    }
    
    public function allForUserRegister()
    {
        $departaments = $this->repository->all(['id', 'name']);
        return $departaments !== null ? $departaments : null;
    }

    public function getOne($id)
    {
        $departament = $this->repository->getById($id);
        return $departament !== null ? new DepartmentResource($departament) : null;
    }

    public function update($id, $data)
    {
        $fields = $this->repository->makeModel()->fill($data->only($this->requestOnly()));
        $departament = $this->repository->makeModel()->find($id);
        return $departament !== null ? $this->repository->updateById($id, $fields->toArray())->fresh() : null;
    }

    public function delete($id)
    {
        $departament = $this->repository->makeModel()->find($id);
        if ( $departament !== null ) {
            $this->repository->deleteById($id);
            return true;
        } else {
            return false;
        }
    }

}
