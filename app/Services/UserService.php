<?php

namespace App\Services;

use App\Repositories\UserRepository;
use \Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

/**
 * Description of UserService
 *
 * @author eder
 */
class UserService
{

    protected $userRepository;

    function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('users');
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithToken($user, $token)
    {
        $data = new \DateTime('now');
        $minutos = $this->guard()->factory()->getTTL() * 2;
        $interval = new \DateInterval('PT'.$minutos.'M');
                
        return response()->json([
                    'user' => [
                        'profile' => $user,
                        'access' => [
                            'token' => $token,
                            'token_type' => 'Bearer ',
                            'expires_in' => $data->add($interval)->format('Y-m-d H:i:s'),
                        ],
                    ],
                        ], Response::HTTP_OK);
    }

    private function requestOnly()
    {
        return $this->userRepository->makeModel()->getFillable();
    }

    public function getUserByEmail($email)
    {
        return $this->userRepository->where('email', 'LIKE', $email)
                        ->get(['id', 'name', 'email', 'department_id', 'status', 'isAdmin'])->first();
    }

    public function store($data)
    {
        $fields = $this->userRepository->makeModel()->fill($data->only($this->requestOnly()));
        $fields->password = Hash::make($fields->password);
        return $this->userRepository->create($fields->toArray());
    }

    public function all()
    {
        $users = $this->userRepository->with('department')->all();
        return UserResource::collection($users);
    }

    public function getOne($id)
    {
        $user = $this->userRepository->with('department')->getById($id);
        return new UserResource($user);
    }

    public function update($data, $id)
    {
        $user = $this->userRepository->makeModel()->find($id);
        if ( $user != null ) {
            $fields = $this->userRepository->makeModel()->fill($data->only($this->requestOnly()));
            return $this->userRepository->updateById($id, $fields->toArray())->fresh();
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $user = $this->userRepository->makeModel()->find($id);
        if ( $user != null ) {
            $this->userRepository->deleteById($id);
            return true;
        } else {
            return false;
        }
    }

}
