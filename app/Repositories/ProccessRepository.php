<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\ProccessModel;

/**
 * Class ProccessRepository.
 */
class ProccessRepository extends BaseRepository
{

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return ProccessModel::class;
    }

}
