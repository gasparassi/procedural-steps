<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\DepartmentModel;

/**
 * Class DepartamentRepository.
 */
class DepartmentRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return DepartmentModel::class;
    }
}
