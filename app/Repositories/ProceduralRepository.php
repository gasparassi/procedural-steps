<?php

namespace App\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\ProceduralModel;

/**
 * Class TramitesRepository.
 */
class ProceduralRepository extends BaseRepository
{

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return ProceduralModel::class;
    }

}
